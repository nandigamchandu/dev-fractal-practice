import React from 'react'
import { Simple } from 'technoidentity-devfractal'

export const Sample: React.FC = () => (
  <Simple.Form
    initialValues={{ userName: '' }}
    onSubmit={(values: any) => {
      // tslint:disable-next-line:no-console
      console.log(values)
    }}
  >
    <Simple.Text name="userName" />
    <Simple.FormButtons reset={false} />
  </Simple.Form>
)
